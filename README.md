# krogue
A roguelike videogame

## Downloading
Clone from this repo.

## Setting up and running
### Global dependencies
```sh
npm install -g grunt-cli

npm install -g coffee-script # (optional)
```

### Game itself
```sh
npm install
grunt
node .
```

Alternatively, if `coffee-script` is globally installed, there is no need to build the project to JavaScript; you can just run `coffee .` after installing dependencies.